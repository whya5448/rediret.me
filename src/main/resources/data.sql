INSERT IGNORE INTO users (email, is_account_non_expired, is_account_non_locked, is_credentials_non_expired, is_enabled, is_valid_email, join_date, NAME, PASSWORD, sign_type)
VALUES ("rediret@rediret.me", 0, 0, 0, 0, 0, now(), "rediret.me", NULL, 0);
INSERT IGNORE INTO users (email, is_account_non_expired, is_account_non_locked, is_credentials_non_expired, is_enabled, is_valid_email, join_date, NAME, PASSWORD, sign_type)
VALUES ("Whya5448@gmail.com", 1, 1, 1, 1, 1, now(), "Whya5448", "$2a$10$S256wpRnxDt7hN3i9JDO4eDEXhaZO7t/RC1Z5i8i8YEaPlI4RxWI6", 0);
INSERT IGNORE INTO authoritys (authority_id, ROLE, user_id) VALUES (1, 0, 1);
INSERT IGNORE INTO authoritys (authority_id, ROLE, user_id) VALUES (2, 0, 2);
INSERT IGNORE INTO authoritys (authority_id, ROLE, user_id) VALUES (3, 1, 2);