/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

/**
 * Created by 안병길 on 2017-01-13.
 * Whya5448@gmail.com
 */

//noinspection JSUnusedLocalSymbols
(function () {

    const rediretMe = {

        getvalue: function (string) {
            if (typeof string === "string") {
                if (string.startsWith("id_")) {
                    return $("#" + string).val();
                }
            }
            return string;
        },

        ismatch: function (value1, value2) {

            if (value1 === undefined || value2 === undefined) {
                return;
            }
            value1 = rediretMe.getvalue(value1);
            value2 = rediretMe.getvalue(value2);

            return value1 === value2;
        },

        isemail: function (address) {
            let email;
            email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
            return email.test(address);
        },

        hide: function (o) {
            $(o).addClass("hide");
            return o;
        },
        show: function (o) {
            $(o).removeClass("hide");
            return o;
        },
        minlength: function (val, limits) {
            return val.length >= limits;
        },
        maxlength: function (val, limits) {
            return val.length <= limits;
        },
        clearChar: function (string) {
            return string.replace(/[_-]+/gi, "");
        },
        alertDanger: function (string, id) {
            return rediretMe.alert("danger", string, id);
        },
        alertSuccess: function (string, id) {
            return rediretMe.alert("success", string, id);
        },
        alert: function (type, string, id) {

            if (typeof type === "undefined") {
                return;
            }
            if (typeof string === "undefined") {
                return;
            }

            let sId = typeof id === "undefined" ? "" : 'id="' + id + '"';

            let dom = '<div ' + sId + ' class="alert alert-' + type + ' hide" role="alert">' +
                '<button data-dismiss="alert" type="button" class="close">' +
                '<span aria-hidden="true">&times;</span>' +
                '<span class="sr-only">Close</span>' +
                '</button><p>' + string + '</p></div>';
            return $(dom);
        },
        random: function () {
            return Math.random() * Math.pow(10, 17);
        },

        isOK: function (data) {
            return data.status_no === 200;
        },

        isTooManyRequests: function (data) {
            return data.status_no === 429;
        },

        getTime: function (d) {
            return d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes();
        }

    };
    window.$r = rediretMe;


    // http://stove99.tistory.com/46
    Date.prototype.format = function (f) {
        if (!this.valueOf()) {
            return " ";
        }

        var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
        var d = this;

        return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function ($1) {
            switch ($1) {
                case "yyyy":
                    return d.getFullYear();
                case "yy":
                    return (d.getFullYear() % 1000).zf(2);
                case "MM":
                    return (d.getMonth() + 1).zf(2);
                case "dd":
                    return d.getDate().zf(2);
                case "E":
                    return weekName[d.getDay()];
                case "HH":
                    return d.getHours().zf(2);
                case "hh":
                    return ((h = d.getHours() % 12) ? h : 12).zf(2);
                case "mm":
                    return d.getMinutes().zf(2);
                case "ss":
                    return d.getSeconds().zf(2);
                case "a/p":
                    return d.getHours() < 12 ? "오전" : "오후";
                default:
                    return $1;
            }
        });
    };
    String.prototype.string = function (len) {
        var s = '', i = 0;
        while (i++ < len) {
            s += this;
        }
        return s;
    };
    String.prototype.zf = function (len) {
        return "0".string(len - this.length) + this;
    };
    Number.prototype.zf = function (len) {
        return this.toString().zf(len);
    };

})();

