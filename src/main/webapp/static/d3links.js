/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

/**
 * Created by 안병길 on 2017-01-13.
 * Whya5448@gmail.com
 */

//noinspection JSUnusedLocalSymbols
(function () {

    window.$r.d3 = {
        date: function (id, data, width, height) {

            let svg = d3.select(id).append("svg").attr("class", "date").attr("width", width).attr("height", height),
                margin = {top: 20, right: 20, bottom: 50, left: 50},
                g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            width = +width - margin.left - margin.right;
            height = +height - margin.top - margin.bottom;

            let x = d3.scaleTime().rangeRound([0, width]);
            let y = d3.scaleLinear().rangeRound([height, 0]);

            let area = d3.area()
                .x(function (d) {
                    return x(d.date);
                })
                .y1(function (d) {
                    return y(d.count);
                });

            tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function (d) {
                    return "<strong>Clicks:</strong> <span style='color:red'>" + d.count + "</span>";
                });

            svg.call(tip);

            data.forEach(v => v.date = new Date(v.date));

            //x.domain(d3.extent(data, function(d) { return d.date; })).ticks(d3.timeWeek);
            x.domain([new Date().setTime(Date.now() - 3600 * 24 * 7 * 1000), new Date()]);

            let tmpData = x.ticks(168).map(function (a) {
                return {"date": a, count: 0};
            });
            data.forEach(function (d) {
                let less = {"target": null, "diff": 0},
                    date = new Date(d.date).getTime();

                tmpData.forEach(function (t) {
                    let tdate = t.date.getTime();
                    let diff = Math.abs(date - tdate);
                    if (less.target == null || diff < less.diff) {
                        less.target = t;
                        less.diff = diff
                    }
                });
                less.target.count++;
            });
            data = tmpData;
            y.domain([-0.03, Math.round(d3.max(data, function (d) {
                    return d.count;
                }) * 1.2)]);
            area.y0(y(0));

            g.append("path")
                .datum(data)
                .attr("fill", "#4285f4")
                .attr("fill-opacity", ".3")
                .attr("stroke", "#4285f4")
                .attr("stroke-linejoin", "round")
                .attr("stroke-linecap", "round")
                .attr("stroke-width", 2)
                .attr("d", area);

            g.append("g")
                .attr("transform", "translate(0," + height + ")")
                .attr("class", "bottom")
                .call(d3.axisBottom(x).ticks(7).tickSizeInner(-height).tickSizeOuter(0).tickPadding(10))
                .selectAll("text")
                .attr("transform", "rotate(-90)")
                .attr("dx", "-1em")
                .attr("dy", "-.5em")
                .style("text-anchor", "end")
                .select(".domain")
                .remove();

            g.append("g")
                .attr("class", "left")
                .call(d3.axisLeft(y).ticks(4).tickSizeInner(-width)
                    .tickSizeOuter(0)
                    .tickPadding(10))
                .append("text")
                .attr("fill", "#000")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", "0.71em")
                .attr("dx", "-0.8em")
                .attr("text-anchor", "end")
                .text("Clicks");
        },
        browserAndPlatform: function (id, data, width, height) {

            let labelName = Object.keys(data[0])[0],
                margin = {top: 0, right: 20, bottom: 0, left: 80},
                svg = d3.select(id).append("svg").attr("class", "browser platform").attr("width", width).attr("height", height + 30);

            width = +width - margin.left - margin.right;
            height = +height - margin.top - margin.bottom;

            let x = d3.scaleLinear().rangeRound([0, width]),
                y = d3.scaleBand().rangeRound([height, 0]).padding(0.1),
                g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")"),
                tip = d3.tip()
                    .attr('class', 'd3-tip')
                    .offset([-10, 0])
                    .html(function (d) {
                        return "<strong>Access:</strong> <span style='color:red'>" + d.AccessCount + "</span>";
                    });

            svg.call(tip);

            x.domain([0, d3.max(data, function (d) {
                return d.AccessCount;
            })]);
            y.domain(data.map(function (d) {
                return d[labelName];
            }));

            g.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));

            g.append("g")
                .call(d3.axisLeft(y));

            g.selectAll(".bar")
                .data(data)
                .enter().append("rect")
                .attr("fill", "#4285f4")
                .attr("class", "bar")
                .attr("y", function (d) {
                    return y(d[labelName]);
                })
                .attr("width", function (d) {
                    return x(d.AccessCount);
                })
                .attr("height", y.bandwidth())
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide);
        },

        referrer: function (id, data, width, height) {

            function midAngle(d) {
                return d.startAngle + (d.endAngle - d.startAngle) / 2;
            }

            let radius = Math.min(width / 2, height / 2);
            let components = ["slices", "labels", "values", "lines"];
            let svg = d3.select(id).append("svg").attr("class", "referrer").attr("width", width).attr("height", height).append("g");
            svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
            components.forEach(c => svg.select("g." + c).empty() ? svg.append("g").attr("class", c) : null);

            let pie = d3.pie()
                .sort(function (a, b) {
                    return a.access < b.access;
                })
                .value(function (d) {
                    return d.access;
                });

            let arc = d3.arc()
                .outerRadius(radius * 0.6)
                .innerRadius(radius * 0.45);

            let key = function (d) {
                return d.data.host;
            };
            let color = d3.scaleOrdinal().range("#4285f4 #a2d200 #fe9900 #f0d202 #994499 #dd4477".split(" "));

            /!* ------- PIE SLICES -------*!/;
            let slice = svg.select(".slices").selectAll("path.slice")
                .data(pie(data), key);

            slice.enter()
                .insert("path")
                .style("fill", function (d) {
                    return color(d.data.host);
                })
                .attr("class", "slice")
                .attr("d", arc);

            /!* ------- TEXT LABELS -------*!/;

            let text = svg.select(".labels").selectAll("text").data(pie(data), key);
            let values = svg.select(".values").selectAll("text").data(pie(data), key);

            let transform = function (d) {
                let pos = arc.centroid(d);
                pos[0] = radius * 1.2 * (midAngle(d) < Math.PI ? 1 : -1);
                return "translate(" + pos + ")";
            };

            let textAnchor = function (d) {
                return midAngle(d) >= Math.PI ? "start" : "end";
            };

            text.enter()
                .append("text")
                .attr("dy", "-0.4em")
                .attr("font-size", "12px")
                .attr("stroke", "none")
                .attr("stroke-width", "0")
                .attr("transform", transform)
                .style("text-anchor", textAnchor)
                .text(function (d) {
                    return d.data.host;
                });

            values.enter()
                .append("text")
                .attr("dy", "1.2em")
                .attr("font-size", "12px")
                .attr("stroke", "none")
                .attr("stroke-width", "0")
                .attr("transform", transform)
                .style("text-anchor", textAnchor)
                .text(function (d) {
                    return d.data.access;
                });

            /!* ------- SLICE TO TEXT POLYLINES -------*!/;

            svg.select(".lines")
                .selectAll("polyline")
                .data(pie(data), key)
                .enter()
                .append("polyline")
                .attr("points", function (d) {
                    return [arc.centroid(d), [radius * 1.2 * (midAngle(d) < Math.PI ? 1 : -1), arc.centroid(d)[1]]];
                });

        }

    };

})();

