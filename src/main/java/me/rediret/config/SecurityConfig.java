/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.config;

import me.rediret.auth.AuthProvider;
import me.rediret.auth.SigninFailureHandler;
import me.rediret.auth.SigninSuccessHandler;
import me.rediret.auth.oauth.ClientResources;
import me.rediret.auth.oauth.GoogleUserInfoTokenService;
import me.rediret.auth.user.session.token.SessionLoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.CompositeFilter;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 안병길 on 2017-01-03.
 * Whya5448@gmail.com
 */
@Configuration
@EnableOAuth2Client
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final AuthProvider authProvider;
	private final SigninSuccessHandler signinSuccessHandler;
	private final SigninFailureHandler signinFailureHandler;
	private final OAuth2ClientContext oAuth2ClientContext;
	private final SessionLoginFilter sessionLoginFilter;

	@SuppressWarnings("SpringJavaAutowiringInspection")
	@Autowired
	public SecurityConfig(BCryptPasswordEncoder bCryptPasswordEncoder, AuthProvider authProvider, SigninSuccessHandler signinSuccessHandler, SigninFailureHandler signinFailureHandler, OAuth2ClientContext oAuth2ClientContext, SessionLoginFilter sessionLoginFilter) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.authProvider = authProvider;
		this.signinSuccessHandler = signinSuccessHandler;
		this.signinFailureHandler = signinFailureHandler;
		this.oAuth2ClientContext = oAuth2ClientContext;
		this.sessionLoginFilter = sessionLoginFilter;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.userDetailsService()).passwordEncoder(bCryptPasswordEncoder);
		auth.authenticationProvider(authProvider);
	}

	@Override
	public void configure(WebSecurity web) {
		// 글로벌 설정
		web.ignoring().antMatchers("/static/**");
		//web.ignoring().anyRequest();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.csrf().disable()
				.formLogin()
				.loginPage("/sign/in")
				.loginProcessingUrl("/sign/in")
				.usernameParameter("email")
				.successHandler(signinSuccessHandler)
				.failureHandler(signinFailureHandler)
				.and()
				.authorizeRequests()
				//.anyRequest().permitAll()
				.antMatchers("/*").permitAll()
				.antMatchers("/url/**").permitAll()
				.antMatchers("/r/**").permitAll()
				.antMatchers("/sign/**").permitAll()
				// hasRole과 hasAuthority는 다름.
				.antMatchers("/admin/**").hasAuthority("ADMIN")
				.anyRequest().authenticated()
				.and()
				.exceptionHandling()
				.accessDeniedPage("/error/403")
				.and()
				.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher(("/sign/out")))
				.logoutSuccessUrl("/sign/in")
				.and()
				.addFilterBefore(sessionLoginFilter, BasicAuthenticationFilter.class)
				.addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class);
	}

	@Bean
	@ConfigurationProperties("google")
	public ClientResources google() {
		return new ClientResources();
	}

	@Bean
	public FilterRegistrationBean oauth2ClientFilterRegistration(OAuth2ClientContextFilter filter) {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(filter);
		registration.setOrder(-100);
		return registration;
	}

	private Filter ssoFilter() {

		CompositeFilter filter = new CompositeFilter();
		List<Filter> filters = new ArrayList<>();
		filters.add(ssoFilter(google(), "/sign/google"));
		filter.setFilters(filters);
		return filter;

	}

	private Filter ssoFilter(ClientResources client, String path) {
		/*
		OAuth2ClientAuthenticationProcessingFilter filter = new OAuth2ClientAuthenticationProcessingFilter(path);
		OAuth2RestTemplate template = new OAuth2RestTemplate(client.getClient(), oAuth2ClientContext);
		UserInfoTokenServices tokenServices = new UserInfoTokenServices(client.getResource().getUserInfoUri(), client.getClient().getClientId());
		tokenServices.setRestTemplate(template);
		filter.setRestTemplate(template);
		filter.setTokenServices(tokenServices);
		*/

		OAuth2ClientAuthenticationProcessingFilter filter = new OAuth2ClientAuthenticationProcessingFilter(path);
		filter.setRestTemplate(new OAuth2RestTemplate(client.getClient(), oAuth2ClientContext));
		filter.setTokenServices(new GoogleUserInfoTokenService(client.getResource().getUserInfoUri(), client.getClient().getClientId()));
		filter.setAuthenticationSuccessHandler(signinSuccessHandler);
		return filter;
	}

}
