/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.utils.exception.auth.account;

import org.springframework.security.authentication.AccountStatusException;

/**
 * Created by 안병길 on 2017-04-11.
 * Whya5448@gmail.com
 */
public class EmailNotVerifiedException extends AccountStatusException {

	public EmailNotVerifiedException(String msg) {
		super(msg);
	}

	public EmailNotVerifiedException(String msg, Throwable t) {
		super(msg, t);
	}
}
