/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Created by 안병길 on 2017-04-11.
 * Whya5448@gmail.com
 */

@Component
@Slf4j
public class MailManager {

	// TODO 자바메일
	private final JavaMailSenderImpl javaMailSender = null;
/*
//TODO 자바메일
	@SuppressWarnings("SpringJavaAutowiringInspection")
	@Autowired
	public MailManager(JavaMailSenderImpl javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	@Async
	public void send(SimpleMailMessage simpleMailMessage) {
		if (StringUtils.isEmpty(simpleMailMessage.getTo())) {
			log.error("empty mail receiver: " + simpleMailMessage.toString());
			return;
		}
		javaMailSender.send(simpleMailMessage);
	}
*/

}
