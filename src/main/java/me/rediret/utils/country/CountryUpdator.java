/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.utils.country;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.net.URL;

/**
 * Created by 안병길 on 2017-03-22.
 * Whya5448@gmail.com
 */
@Component
@Slf4j
public class CountryUpdator {

	private final ObjectMapper objectMapper;

	@Value("${app.whois.kisa.token}")
	private String token;

	@Value("${app.whois.kisa.format}")
	private String format;

	@Value("${app.whois.kisa.path}")
	private String path;

	@Value("${app.whois.kisa.domain}")
	private String domain;

	@Autowired
	public CountryUpdator(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Async
	public void update(CountryLogs countryLogs, CountryManager manager) {
		try {
			String query = path + "?key=" + token + "&answer=" + format + "&query=" + countryLogs.getIp();
			URL url = new URL("http", domain, 80, query);
			KisaWhois whois = objectMapper.readValue(url, KisaWhois.class);
			countryLogs.setCountry(whois.getCountryCode());
			manager.saveAndFlush(countryLogs);
		} catch (Exception e) {
			log.error(e.getMessage());
			for (StackTraceElement stackTraceElement : e.getStackTrace()) log.error(stackTraceElement.toString());

			e.printStackTrace();
		}
	}
}
