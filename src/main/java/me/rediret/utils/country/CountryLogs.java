/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.utils.country;

/**
 * Created by 안병길 on 2017-03-22.
 * Whya5448@gmail.com
 */
public interface CountryLogs {
	void setCountry(String country);

	String getIp();
}
