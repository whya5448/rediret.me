/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.utils.country;

import lombok.Setter;

import java.util.Map;

/**
 * Created by 안병길 on 2017-03-22.
 * Whya5448@gmail.com
 */

class KisaWhois {
	@Setter
	private Map<String, String> whois;
/*
	String getQuery() { return whois.get("query"); }
	String getQueryType() { return whois.get("queryType"); }
	String getRegistry() { return whois.get("registry"); }
*/

    String getCountryCode() {
        if (whois.containsKey("error")) return "XX";
        var code = whois.get("countryCode");
        return code.equals("none") ? "XX" : String.valueOf(code);
    }

	@Override
	public String toString() {
		return getCountryCode();
	}
}
