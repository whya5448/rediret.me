/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Created by 안병길 on 2017-02-18.
 * Whya5448@gmail.com
 */
@Component
public class Utils {

    private final static List<String> REAL_IP_HEADER_NAMES = List.of("x-real-ip", "x-forwarded-for");
    private final ResourceBundleMessageSource resourceBundleMessageSource;

    @Autowired
    public Utils(ResourceBundleMessageSource resourceBundleMessageSource) {
        this.resourceBundleMessageSource = resourceBundleMessageSource;
    }

    /**
     * 실서버 nginx 리버스 프록시. X-Real-IP에서 실IP 가져옴.
     *
     * @param request the request
     * @return the real ip
     */
    public static String getRealIp(HttpServletRequest request) {

        var ip = request.getRemoteAddr();

        var headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement().toLowerCase();
            if (REAL_IP_HEADER_NAMES.stream().anyMatch(x -> x.toLowerCase().equals(headerName))) {
                ip = request.getHeader(headerName);
                break;
            }
        }

        return ip;
    }

    /**
     * 이메일 서비스용 메시지 프리셋
     *
     * @return the simple mail message
     */
    public static SimpleMailMessage getSimpleMailMessage() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setReplyTo("whya5448@gmail.com");
        simpleMailMessage.setFrom("no-reply@rediret.me");
        simpleMailMessage.setSentDate(new Date());
        return simpleMailMessage;
    }

    public String getLocaleMessage(String code) {
        return resourceBundleMessageSource.getMessage(code, null, LocaleContextHolder.getLocale());
    }
}
