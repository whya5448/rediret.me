/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.utils;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 안병길 on 2017-02-11.
 * Whya5448@gmail.com
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Message extends HashMap<String, Object> {

	public void setStatus(HttpStatus httpStatus) {
		this.put("status", httpStatus.getReasonPhrase());
		this.put("status_no", httpStatus.value());
	}

	public void setMessage(String message) {
		this.put("message", message);
	}

	public Object put(String key, Object object) {
		super.put("timestamp", new Date());
		return super.put(key, object);
	}

	public String toJson() {
		String x = "{";
		for (Map.Entry<String, Object> entry : super.entrySet()) {
			Object value = (value = entry.getValue()) instanceof Date ? ((Date) value).getTime() : value instanceof Number ? value : "\"" + value + "\"";
			x += ",\"" + entry.getKey() + "\"" + ":" + value;
		}
		x += "}";
		return x.replaceFirst(",", "");
	}

}