/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * The type Shorturl application.
 */
@SpringBootApplication
//@EnableAuthorizationServer
//@EnableResourceServer
public class RediretApplication {
    public static void main(String[] args) {
        SpringApplication.run(RediretApplication.class, args);
    }
}