/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.url;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by 안병길 on 2017-01-01.
 * Whya5448@gmail.com
 */
@Slf4j
@Component
class UrlWorker {

	private final int BASE = 64;
	private final String Codes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
    private final SecureRandom sr = UrlWorker.getSecureRandom();

    private static SecureRandom getSecureRandom() {
        SecureRandom sr;
        try {
            sr = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            sr = new SecureRandom();
            log.warn("알고리즘 없음.");
        }
        if (sr == null) log.error("SecureRandom 생성실패함!");
        return sr;
    }

	/**
	 * Gets rand int.
	 *
	 * @param Max the max
	 * @return the rand int
	 */
	int getRandInt(int Max) {
        return sr.nextInt(Max);
    }

	/**
	 * Gets codes.
	 *
	 * @param Number the number
	 * @return the codes
	 */
	String getCodes(int Number) {
        ArrayList<Integer> reminder = new ArrayList<>();
        String code = "";

        // Number가 0, 1
        if(Number < 2) reminder.add(Number);

        // Number가 2 이상일 경우
        while(Number > 1) {

            // 나머지를 먼저 집어넣고
            reminder.add(Number % BASE);

            // 몇으로 나눔값 집어넣고 갱신
            Number = Number / BASE;

            // 다 나누면 집어넣고 탈출
            if(Number == 1) {
                reminder.add(1);
                break;
            }
        }

        // 역정렬
        Collections.reverse(reminder);

        // 진수에서 나눈값 가져와서 문자열에 붙힘.
        for(int x : reminder) code += Codes.charAt(x);

        // 반환
        return code;
    }

    private static class Singleton {
	    /**
	     * The Instance.
	     */
	    static final UrlWorker instance = new UrlWorker();
    }
}