/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.url;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by 안병길 on 2017-01-01.
 * Whya5448@gmail.com
 */
interface UrlRepository extends JpaRepository<URL, Integer> {

	/**
	 * Find by short url url.
	 *
	 * @param shortUrl the short url
	 * @return the url
	 */


	List<URL> findByShortUrl(String shortUrl);

	Page<URL> findByUserId(int userId, Pageable pageable);

	Page<URL> findByUserIdAndLongUrlContainingOrShortUrlContaining(int userId, String keyword, String keyword2, Pageable pageable);

	Long countBycreatedDateGreaterThan(Date date);

}