/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.url.logs;

import me.rediret.url.URL;
import me.rediret.utils.country.CountryLogs;
import me.rediret.utils.country.CountryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 안병길 on 2017-03-22.
 * Whya5448@gmail.com
 */

@Component
public class RedirectURLAccessManager implements CountryManager {

	private final RedirectURLAccessRepository redirectURLAccessRepository;
	private final Worker worker = new Worker();

	@Autowired
	public RedirectURLAccessManager(RedirectURLAccessRepository redirectURLAccessRepository) {
		this.redirectURLAccessRepository = redirectURLAccessRepository;
	}

	@Override
	public CountryLogs saveAndFlush(CountryLogs logs) {
		return redirectURLAccessRepository.saveAndFlush((RedirectURLAccessLogs) logs);
	}

	public List<Map<String, Object>> getReferrerByURL(URL url) {
		List<Map<String, Object>> list = new ArrayList<>();
		List<Object[]> o = redirectURLAccessRepository.findCountAndReferrerByURL(url.getId());
		for (Object[] oo : o) {
			Map<String, Object> map = new HashMap<>();
			map.put("access", oo[0]);
			map.put("host", oo[1]);
			list.add(map);
		}
		return list;
	}


	// <!--------------------- D3 ---------------------!>
	// D3를 위한 분석 메소드는 쿼리 직접사용하므로 prefix get으로 차별화함.

	public List<Map<String, Integer>> getAccessDateByURL(URL url) {
		return redirectURLAccessRepository.findAccessDateByURL(url);
	}

	public Map<String, Object> getUserAgentAndPlatformByURL(URL url) {
		List<Map<String, Object>> list = redirectURLAccessRepository.findUserAgentByURL(url);
		Map<String, Object> map = new HashMap<>();
		Map<String, Long> platforms = new HashMap<>(),
				browsers = new HashMap<>();
		List<Object> d3platforms = new ArrayList<>(),
				d3browsers = new ArrayList<>();
		for (Map<String, Object> m : list) {
			String ua = (String) m.get("userAgent");
			Long accessCount = (Long) m.get("accessCount");
			String platform = worker.getPlatform(ua);
			String browser = worker.getBrowser(ua);

			if (platforms.containsKey(platform)) platforms.put(platform, platforms.get(platform) + accessCount);
			else platforms.put(platform, accessCount);

			if (browsers.containsKey(browser)) browsers.put(browser, browsers.get(browser) + accessCount);
			else browsers.put(browser, accessCount);
		}

		browsers.forEach((k, v) -> {
			Map<String, Object> tmp = new HashMap<>();
			tmp.put("Browser", k);
			tmp.put("AccessCount", v);
			d3browsers.add(tmp);
		});

		platforms.forEach((k, v) -> {
			Map<String, Object> tmp = new HashMap<>();
			tmp.put("Platform", k);
			tmp.put("AccessCount", v);
			d3platforms.add(tmp);
		});

/*
		// D3에서 받아들이는 형식이 아님.
		map.put("Platforms", platforms);
		map.put("Browsers", browsers);
*/

		map.put("d3Platforms", d3platforms);
		map.put("d3Browsers", d3browsers);
		return map;
	}

	private class Worker {

		Data[] platforms = {
				new Data("windows", "Windows"),
				new Data("android", "Android"),
				new Data("iphone", "iPhone"),
				new Data("linux", "Linux")
		};
		Data[] browsers = {
				new Data("trident", "IE"),
				new Data("edge", "Edge"),
				new Data("chrome", "Chrome"),
				new Data("safari", "Safari"),
				new Data("firefox", "Firefox")
		};

		String getPlatform(String userAgent) {
			for (Data data : platforms) if (userAgent.toLowerCase().contains(data.getCondition())) return data.getValue();
			return "unknown";
		}

		String getBrowser(String userAgent) {
			for (Data data : browsers) if (userAgent.toLowerCase().contains(data.getCondition())) return data.getValue();
			return "unknown";
		}

		@lombok.Data
		private class Data {
			private String condition;
			private String value;

			Data(String condition, String value) {
				this.condition = condition;
				this.value = value;
			}

		}
	}

	// <!--------------------- D3 ---------------------!>

}
