/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.url.logs;

import lombok.Data;
import me.rediret.url.URL;
import me.rediret.utils.Utils;
import me.rediret.utils.country.CountryLogs;

import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by 안병길 on 2017-03-17.
 * Whya5448@gmail.com
 */

@Entity
@Data
@Table(name = "access_logs")
public class RedirectURLAccessLogs implements CountryLogs {

    @Id
    @GeneratedValue
    @Column(name = "access_log_id")
    private int id;
    @Column(nullable = false)
    private Date accessDate = new Date();
    @Column(nullable = false, length = 15)
    private String ip;
    @Column(nullable = false)
    private String userAgent;
    @Column(length = 2)
    private String country;
    @Column
    private String referrer;
    @ManyToOne(cascade = CascadeType.MERGE, optional = false)
    @JoinColumn(name = "url_id")
    private URL URL;

    public RedirectURLAccessLogs() {
    }

    public RedirectURLAccessLogs(HttpServletRequest request, URL url) {
        set(request, url);
    }

    public void set(HttpServletRequest request, URL url) {
        this.set(request);
        this.URL = url;
    }

    public void set(HttpServletRequest request) {
        this.ip = Utils.getRealIp(request);
        try {
            this.referrer = request.getHeader("referrer");
        } catch (Exception ignored) {
        }
        try {
            this.userAgent = request.getHeader("user-agent");
        } catch (Exception ignored) {
        }
    }


}
