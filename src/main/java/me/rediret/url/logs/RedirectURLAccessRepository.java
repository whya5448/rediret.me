/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.url.logs;

import me.rediret.url.URL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

/**
 * Created by 안병길 on 2017-01-01.
 * Whya5448@gmail.com
 */
interface RedirectURLAccessRepository extends JpaRepository<RedirectURLAccessLogs, Integer> {
	@Query("select count(r.accessDate) as accessCount, r.accessDate as date from RedirectURLAccessLogs r where r.URL = ?1 group by accessDate order by accessDate")
	List<Map<String, Integer>> findAccessDateByURL(URL url);

	@Query("SELECT count(r.userAgent) as accessCount, userAgent as userAgent FROM RedirectURLAccessLogs r where r.URL = ?1 group by userAgent order by accessCount desc")
	List<Map<String, Object>> findUserAgentByURL(URL url);

	@Query(value = "SELECT COUNT(0) AS access, ifnull(SUBSTRING_INDEX(REGEXP_REPLACE(referrer,'https?://',''),'/',1),\"unknown\") AS referrer FROM access_logs WHERE url_id = ?1 GROUP BY SUBSTRING_INDEX(REGEXP_REPLACE(referrer,'https?://',''),'/',1) order by access desc", nativeQuery = true)
	List<Object[]> findCountAndReferrerByURL(int url);
}