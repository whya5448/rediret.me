/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.url;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import me.rediret.auth.user.User;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 안병길 on 2017-01-01.
 * Whya5448@gmail.com
 */
public
@Entity(name = "urls")
@Data
@Component
class URL {

	@Id
	@GeneratedValue
	@Column(name = "url_id")
	private int id;

	@Column(nullable = false)
	private String longUrl;

	@Column(nullable = false)
	private String shortUrl;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastAccess;

	@Column(nullable = false)
	private String ip;

	private int accessCount = 0;

	@ManyToOne(cascade = CascadeType.MERGE, optional = false)
	@JoinColumn(name = "user_id")
	@JsonIgnore
	private User user;

}