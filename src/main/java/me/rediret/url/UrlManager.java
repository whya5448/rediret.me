/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.url;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by 안병길 on 2017-01-01.
 * Whya5448@gmail.com
 */
@Component
public class UrlManager {

    private final UrlWorker urlWorker;
    private final UrlRepository urlRepository;

    @Autowired
    public UrlManager(UrlWorker urlWorker, UrlRepository urlRepository) {
        this.urlWorker = urlWorker;
        this.urlRepository = urlRepository;
    }

    /**
     * Gets rand int.
     *
     * @param max the max
     * @return the rand int
     */
    public int getRandInt(int max) {
        return urlWorker.getRandInt(max);
    }

    /**
     * Compress string.
     *
     * @param number the number
     * @return the string
     */
    public String compress(int number) {
        return urlWorker.getCodes(number);
    }

    public boolean exists(int urlId) {
    	//TODO 수정
        return true;
        //return urlRepository.exists(urlId);
    }

    public URL getOne(int urlId) {
        return urlRepository.getOne(urlId);
    }

    public URL save(URL url) {
        return urlRepository.save(url);
    }

    public List<URL> findByShortUrl(String shortUrl) {
        return urlRepository.findByShortUrl(shortUrl);
    }

    public Long countBycreatedDateGreaterThan(Date date) {
        return urlRepository.countBycreatedDateGreaterThan(date);
    }

    public Page<URL> findByUserId(int userId, Pageable pageable) {
        return urlRepository.findByUserId(userId, pageable);
    }

    public Page<URL> findByUserIdAndLongUrlContainingOrShortUrlContaining(int userId, String keyword, Pageable pageable) {
        return urlRepository.findByUserIdAndLongUrlContainingOrShortUrlContaining(userId, keyword, keyword, pageable);
    }

    public URL saveAndFlush(URL url) {
        return urlRepository.saveAndFlush(url);
    }
}
