/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.oauth;

import me.rediret.auth.user.User;
import me.rediret.auth.user.UserManager;
import me.rediret.auth.user.logs.SignLogs;
import me.rediret.auth.user.logs.SignLogsManager;
import me.rediret.utils.country.CountryUpdator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;

/**
 * Created by 안병길 on 2017-02-22.
 * Whya5448@gmail.com
 */

@Component
public class OAuth2SuccessHandler implements AuthenticationSuccessHandler {


	private final UserManager userManager;
	private final SignLogs signLogs;
	private final SignLogsManager signLogsManager;
	private final CountryUpdator countryUpdator;

	@Autowired
	public OAuth2SuccessHandler(UserManager userManager, SignLogs signLogs, SignLogsManager signLogsManager, CountryUpdator countryUpdator) {
		this.userManager = userManager;
		this.signLogs = signLogs;
		this.signLogsManager = signLogsManager;
		this.countryUpdator = countryUpdator;
	}

	@Override
	@Transactional
	public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth) throws IOException, ServletException {

		// 왠지 모르겠는데 AuthoritesExtractor 설정해도 넘어오면 null로 넘어옴. 그냥 권한 새로 생성.

		// 연결되어 있는 계정이 있는지 확인합니다.
		// 이전 강의 AccountService.getAccountByOAuthId 참고!!

		User user = userManager.findByEmail(((User) auth.getPrincipal()).getEmail());

		// 연결되어 있는 계정이 있는경우.
		if (user != null) {
			// 기존 인증을 바꿉니다.
			// 이전 강의의 일반 로그인과 동일한 방식으로 로그인한 것으로 간주하여 처리합니다.
			// 기존 인증이 날아가기 때문에 OAUTH ROLE은 증발하며, USER ROLE 이 적용됩니다.
			SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities()));
			res.sendRedirect("/");
		}
		// 연결된 계정이 없는경우
		else {

			user = (User) auth.getPrincipal();
			user.setValidEmail(true);
			user = userManager.signUp(user);

			SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities()));
			res.sendRedirect("/");

		}

		// 알수 없는 이유로 스프링이 재사용함.
		signLogs.set(req, user);
		signLogs.setId(null);
		signLogs.setSignType(((User) auth.getPrincipal()).getSignType());
		countryUpdator.update(signLogs, signLogsManager);

	}
}
