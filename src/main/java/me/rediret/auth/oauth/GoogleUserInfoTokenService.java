/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.oauth;

import me.rediret.auth.user.SignType;
import me.rediret.auth.user.User;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;

import java.util.Map;

/**
 * Created by 안병길 on 2017-02-22.
 * Whya5448@gmail.com
 */
public class GoogleUserInfoTokenService extends UserInfoTokenServices {


	public GoogleUserInfoTokenService(String userInfoEndpointUrl, String clientId) {
		super(userInfoEndpointUrl, clientId);
	}

	@Override
	protected Object getPrincipal(Map<String, Object> map) {
		User user = new User();
		user.setName((String) super.getPrincipal(map));
		user.setEmail((String) map.get("email"));
		user.setSignType(SignType.GOOGLE);
		user.setEnabled((Boolean) map.get("email_verified"));
		return user;
	}

}
