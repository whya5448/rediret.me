/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth;

import lombok.extern.slf4j.Slf4j;
import me.rediret.auth.user.User;
import me.rediret.auth.user.UserRepository;
import me.rediret.utils.exception.auth.account.EmailNotVerifiedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

/**
 * Created by 안병길 on 2017-01-24.
 * Whya5448@gmail.com
 */

@Slf4j
@Component
public class AuthProvider implements AuthenticationProvider {

	private final UserRepository userRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final ResourceBundleMessageSource resourceBundleMessageSource;

	@Autowired
	public AuthProvider(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, ResourceBundleMessageSource resourceBundleMessageSource) {
		this.userRepository = userRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.resourceBundleMessageSource = resourceBundleMessageSource;
	}

	@Override
	@Transactional
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String user_id = (String) authentication.getPrincipal();
		String user_pw = (String) authentication.getCredentials();

		User user = userRepository.findByEmail(user_id);

		if (user == null || !bCryptPasswordEncoder.matches(user_pw, user.getPassword()))
			// BadCredentialsException -> 왠지 모르게 스택 오버플로우 만듬. loadByUserName을 반복 호출함.
			throw new DisabledException("Incorrect email or password.");

		if (!user.isEnabled()) throw new DisabledException(resourceBundleMessageSource.getMessage("app.sign.in.error.disabled", null, LocaleContextHolder.getLocale()));
		if (!user.isAccountNonExpired()) throw new AccountExpiredException(resourceBundleMessageSource.getMessage("app.sign.in.error.expired", null, LocaleContextHolder.getLocale()));
		if (!user.isAccountNonLocked()) throw new LockedException(resourceBundleMessageSource.getMessage("app.sign.in.error.locked", null, LocaleContextHolder.getLocale()));
		if (!user.isCredentialsNonExpired()) throw new CredentialsExpiredException(resourceBundleMessageSource.getMessage("app.sign.in.error.credentialsExpired", null, LocaleContextHolder.getLocale()));
		if (!user.isValidEmail()) throw new EmailNotVerifiedException(resourceBundleMessageSource.getMessage("app.sign.in.error.emailNotValid", null, LocaleContextHolder.getLocale()));

		log.info("Welcome authenticate! {}", user_id);

		//result.setDetails(new CustomUserDetails(user_id, user_pw));
		return new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
