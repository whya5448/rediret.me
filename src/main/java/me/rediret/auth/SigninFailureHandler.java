/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth;

import me.rediret.utils.Message;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by 안병길 on 2017-02-15.
 * Whya5448@gmail.com
 */
@Component
public class SigninFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
		Message message = new Message();
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		message.setMessage(exception.getMessage());
		message.setStatus(HttpStatus.UNAUTHORIZED);

		PrintWriter writer = response.getWriter();
		writer.print(message.toJson());
		writer.flush();
		writer.close();
	}
}
