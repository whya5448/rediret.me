/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user;

/**
 * Created by 안병길 on 2017-02-20.
 * Whya5448@gmail.com
 */
public enum SignType {
	DEFAULT,
	GOOGLE
}
