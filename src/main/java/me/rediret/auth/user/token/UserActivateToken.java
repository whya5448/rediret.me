/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user.token;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import me.rediret.auth.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.UUID;

/**
 * Created by 안병길 on 2017-01-03.
 * Whya5448@gmail.com
 */
@Entity
@Table(name = "user_token")
@Data
public class UserActivateToken {

	@Id
	@GeneratedValue
	@Column(name = "token_id")
	private int id;

	@ManyToOne(cascade = CascadeType.MERGE, optional = false)
	@JoinColumn(name = "user_id")
	@JsonIgnore
	private User user;

	@Column(nullable = false, unique = true, length = 36)
	private String token;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date expired;

	@Column(nullable = false)
	private TokenState tokenState;

	UserActivateToken() {
	}

	public UserActivateToken(User user) {
		this.user = user;
		this.created = new Date();
		this.expired = Date.from(LocalDateTime.now().plusDays(7).atZone(ZoneId.systemDefault()).toInstant());
		this.token = UUID.randomUUID().toString();
		this.tokenState = TokenState.AVAILABLE;
	}

}
