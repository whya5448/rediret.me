/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by 안병길 on 2017-01-03.
 * Whya5448@gmail.com
 */
@Component
public class UserActivateTokenManager {
	private final UserActivateTokenRepository userActivateTokenRepository;

	@Autowired
	public UserActivateTokenManager(UserActivateTokenRepository userActivateTokenRepository) {
		this.userActivateTokenRepository = userActivateTokenRepository;
	}

	public UserActivateToken findByToken(String token) {
		return userActivateTokenRepository.findByToken(token);
	}

	public UserActivateToken saveAndFlush(UserActivateToken token) {
		return userActivateTokenRepository.saveAndFlush(token);
	}
}