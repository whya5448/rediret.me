/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user.logs;

import lombok.Data;
import me.rediret.auth.user.SignType;
import me.rediret.auth.user.User;
import me.rediret.utils.Utils;
import me.rediret.utils.country.CountryLogs;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by 안병길 on 2017-03-22.
 * Whya5448@gmail.com
 */

@Component
@Entity
@Data
@Table(name = "sign_logs")
public class SignLogs implements CountryLogs {

	@Id
	@GeneratedValue
	@Column(name = "sign_log_id")
	private Integer id;
	@Column(nullable = false)
	private Date signDate = new Date();
	@Column(nullable = false, length = 15)
	private String ip;
	@Column(nullable = false)
	private String userAgent;
	@Column(length = 2)
	private String country;
	@Column(nullable = false)
	private SignType signType = SignType.DEFAULT;
	@ManyToOne(cascade = CascadeType.MERGE, optional = false)
	@JoinColumn(name = "user_id")
	private User user;

	public void set(HttpServletRequest request, User user) {
		this.set(request);
		this.user = user;
		this.signType = user.getSignType();
	}

	public void set(HttpServletRequest request) {
		this.ip = Utils.getRealIp(request);
		try {
			this.userAgent = request.getHeader("user-agent");
		} catch (Exception ignored) {
		}
	}

}
