/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user.logs;

import me.rediret.utils.country.CountryLogs;
import me.rediret.utils.country.CountryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by 안병길 on 2017-03-22.
 * Whya5448@gmail.com
 */

@Component
public class SignLogsManager implements CountryManager {

	private final SignLogsRepository signLogsRepository;

	@Autowired
	public SignLogsManager(SignLogsRepository signLogsRepository) {
		this.signLogsRepository = signLogsRepository;
	}

	@Override
	public CountryLogs saveAndFlush(CountryLogs logs) {
		return signLogsRepository.saveAndFlush((SignLogs) logs);
	}
}
