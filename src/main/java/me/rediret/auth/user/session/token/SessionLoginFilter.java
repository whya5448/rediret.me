package me.rediret.auth.user.session.token;

import lombok.extern.slf4j.Slf4j;
import me.rediret.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Date;

/**
 * Created by 안병길 on 2017-04-23.
 * Whya5448@gmail.com
 */

@Slf4j
@Component
public class SessionLoginFilter implements Filter {

	@Autowired
	private SessionTokenManager sessionTokenManager;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	@Transactional
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		try {
			HttpServletRequest req = (HttpServletRequest) request;

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();

			if (req.getSession().getAttribute("alreadyTriedRememberMe") == null && auth == null && !auth.isAuthenticated()) {
				req.getSession().setAttribute("alreadyTriedRememberMe", true);
				Cookie[] cookies = req.getCookies();
				String token1 = null, token2 = null;
				for (Cookie k : cookies) {
					if (k.getName().equals("token1")) token1 = k.getValue();
					else if (k.getName().equals("token2")) token2 = k.getValue();
				}

				if (token1 != null && token2 != null) {
					SessionToken sessionToken = sessionTokenManager.findByToken(token1, token2);
					if (sessionToken != null) {
						if (Utils.getRealIp(req).equals(sessionToken.getIp()) && sessionToken.getUserAgent().equals(req.getHeader("user-agent"))) {
							log.info("Remember Me: " + sessionToken);
							SecurityContextHolder.getContext().setAuthentication(new RememberMeAuthenticationToken(token1 + token2, sessionToken.getUser(), sessionToken.getUser().getAuthorities()));
							sessionToken.setLastLogin(new Date());
							sessionTokenManager.saveAndFlush(sessionToken);
						}
					}
				}

			}

		} catch (Exception ignored) {}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {

	}
}
