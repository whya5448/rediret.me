/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user.session.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by 안병길 on 2017-04-22.
 * Whya5448@gmail.com
 */

@Component
public class SessionTokenManager {

	private final SessionTokenRepository sessionTokenRepository;

	@Autowired
	public SessionTokenManager(SessionTokenRepository sessionTokenRepository) {
		this.sessionTokenRepository = sessionTokenRepository;
	}

	public SessionToken findByToken(String token1, String token2) {
		return sessionTokenRepository.findByToken1AndToken2(token1, token2);
	}
	public SessionToken saveAndFlush(SessionToken sessionToken) {
		return sessionTokenRepository.saveAndFlush(sessionToken);
	}

}
