/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user.session.token;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import me.rediret.auth.user.User;
import me.rediret.utils.Utils;

import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by 안병길 on 2017-04-22.
 * Whya5448@gmail.com
 */

@Data
@Entity
@Table(name = "session_token")
public class SessionToken {

	@Id
	@GeneratedValue
	@Column(name = "session_token_id")
	private int id;

	@Column(nullable = false, length = 36)
	private String token1;

	@Column(nullable = false, length = 36)
	private String token2;

	@Column(nullable = false, length = 15)
	private String ip;

	@Column(nullable = false)
	private String userAgent;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate = new Date();

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin = new Date();

	@ManyToOne(cascade = CascadeType.MERGE, optional = false)
	@JoinColumn(name = "user_id")
	@JsonIgnore
	private User user;

	public void set(HttpServletRequest request, User user) {
		this.set(request);
		this.user = user;
	}

	public void set(HttpServletRequest request) {
		this.ip = Utils.getRealIp(request);
		try {
			this.userAgent = request.getHeader("user-agent");
		} catch (Exception ignored) {}
	}
}
