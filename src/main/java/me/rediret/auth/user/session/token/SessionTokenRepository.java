/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user.session.token;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by 안병길 on 2017-04-22.
 * Whya5448@gmail.com
 */
interface SessionTokenRepository extends JpaRepository<SessionToken, Integer> {
	SessionToken findByToken1AndToken2(String token1, String token2);
}
