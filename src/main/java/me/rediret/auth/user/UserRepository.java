/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by 안병길 on 2017-01-24.
 * Whya5448@gmail.com
 */
public interface UserRepository extends JpaRepository<User, Integer> {

	User findByEmail(String email);

}
