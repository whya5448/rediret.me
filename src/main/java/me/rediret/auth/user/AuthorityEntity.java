/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user;

import lombok.Data;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Created by 안병길 on 2017-01-03.
 * Whya5448@gmail.com
 */
@Entity
@Data
@ToString(exclude = {"user"})
@Table(name = "Authoritys")
public class AuthorityEntity implements GrantedAuthority {

	@Id
	@GeneratedValue
	@Column(name = "authority_id")
	private int id;

	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "user_id")
	private User user;

	private Roles role;

	public AuthorityEntity() {
	}

	public AuthorityEntity(User user, Roles role) {
		this.user = user;
		this.role = role;
	}

	@Override
	public String getAuthority() {
		return role.toString();
	}
}
