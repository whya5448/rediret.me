/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth.user;

import lombok.extern.slf4j.Slf4j;
import me.rediret.auth.user.token.UserActivateToken;
import me.rediret.auth.user.token.UserActivateTokenManager;
import me.rediret.utils.MailManager;
import me.rediret.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Created by 안병길 on 2017-02-22.
 * Whya5448@gmail.com
 */
@Component
@Slf4j
public class UserManager {

	private final UserRepository userRepository;
	private final AuthorityRepository authorityRepository;
	private final BCryptPasswordEncoder passwordEncoder;
	private final UserActivateTokenManager userActivateTokenManager;
	private final MailManager mailManager;
	private final Utils utils;
	private final Environment environment;

	@Autowired
	public UserManager(UserRepository userRepository, AuthorityRepository authorityRepository, BCryptPasswordEncoder passwordEncoder, UserActivateTokenManager userActivateTokenManager, MailManager mailManager, Utils utils, Environment environment) {
		this.userRepository = userRepository;
		this.authorityRepository = authorityRepository;
		this.passwordEncoder = passwordEncoder;
		this.userActivateTokenManager = userActivateTokenManager;
		this.mailManager = mailManager;
		this.utils = utils;
		this.environment = environment;
	}

	public User saveAndFlush(User user) {
		return userRepository.saveAndFlush(user);
	}

	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	public User signUp(User user) {

		if (user.getPassword() != null) user.setPassword(passwordEncoder.encode(user.getPassword()));
		AuthorityEntity authEntity = new AuthorityEntity(user, Roles.USER);
		System.out.println(user);

		try {
			user = userRepository.saveAndFlush(user);
			authorityRepository.saveAndFlush(authEntity);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityViolationException("");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			log.error(e.getClass().getName());
		}

		user.setAuthorities(AuthorityUtils.createAuthorityList("USER"));
		UserActivateToken userActivateToken = new UserActivateToken(user);
		userActivateTokenManager.saveAndFlush(userActivateToken);

		SimpleMailMessage msg = Utils.getSimpleMailMessage();
		msg.setTo(user.getEmail());
		msg.setText(
				""
						+ utils.getLocaleMessage("app.sign.up.auth.mail.text.prefix")
						+ environment.getProperty("app.host") + "/sign/activate/" + userActivateToken.getToken()
						+ utils.getLocaleMessage("app.sign.up.auth.mail.text.suffix")
		);
		msg.setSubject(utils.getLocaleMessage("app.sign.up.auth.mail.subject"));

		// TODO 자바메일
		//mailManager.send(msg);

		return user;

	}
}
