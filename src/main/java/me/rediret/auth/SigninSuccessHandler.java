/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.auth;

import me.rediret.auth.user.User;
import me.rediret.auth.user.UserManager;
import me.rediret.auth.user.logs.SignLogs;
import me.rediret.auth.user.logs.SignLogsManager;
import me.rediret.auth.user.session.token.SessionToken;
import me.rediret.auth.user.session.token.SessionTokenManager;
import me.rediret.utils.Message;
import me.rediret.utils.country.CountryUpdator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

/**
 * Created by 안병길 on 2017-02-15.
 * Whya5448@gmail.com
 */
@Component
public class SigninSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {


	@Value("${app.login.token.cookie.domain}")
	private String cookieDomain;
	@Value("${app.login.token.cookie.secure}")
	private Boolean cookieSecure;

	private final SessionTokenManager sessionTokenManager;
	private final UserManager userManager;
	private final CountryUpdator countryUpdator;
	private final SignLogsManager signLogsManager;
	private final SignLogs signLogs;

	@Autowired
	public SigninSuccessHandler(SessionTokenManager sessionTokenManager, UserManager userManager, CountryUpdator countryUpdator, SignLogsManager signLogsManager, SignLogs signLogs) {
		this.sessionTokenManager = sessionTokenManager;
		this.userManager = userManager;
		this.countryUpdator = countryUpdator;
		this.signLogsManager = signLogsManager;
		this.signLogs = signLogs;
	}

	@Override
	@Transactional
	public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth) throws ServletException, IOException {

		res.setCharacterEncoding("utf-8");

		SessionToken sessionToken = new SessionToken();
		String uuid = UUID.randomUUID().toString();
		sessionToken.setToken1(uuid);
		Cookie loginToken = new Cookie("token1", uuid);
		loginToken.setDomain(cookieDomain);
		loginToken.setHttpOnly(true);
		loginToken.setSecure(cookieSecure);
		loginToken.setPath("/");
		loginToken.setMaxAge(Integer.MAX_VALUE);
		res.addCookie(loginToken);

		uuid = UUID.randomUUID().toString();
		sessionToken.setToken2(uuid);
		loginToken = new Cookie("token2", uuid);
		loginToken.setDomain(cookieDomain);
		loginToken.setHttpOnly(true);
		loginToken.setSecure(cookieSecure);
		loginToken.setPath("/");
		loginToken.setMaxAge(Integer.MAX_VALUE);
		res.addCookie(loginToken);

		// 일반 로그인
		if(auth.getClass() == UsernamePasswordAuthenticationToken.class) {

			res.setContentType("application/json");
			Message message = new Message();

			signLogs.set(req, (User) auth.getPrincipal());
			sessionToken.set(req, (User) auth.getPrincipal());

			countryUpdator.update(signLogs, signLogsManager);

			message.setMessage("Successfully signed in.");
			message.setStatus(HttpStatus.OK);
			message.put("redirectTo", "/");

			PrintWriter writer = res.getWriter();
			writer.print(message.toJson());
			writer.flush();
			writer.close();

			// OAuth2 인증
		} else {

			User user = userManager.findByEmail(((User) auth.getPrincipal()).getEmail());

			// 연결되어 있는 계정이 있는경우.
			//noinspection StatementWithEmptyBody
			if (user != null) {
				// 기존 인증을 바꿉니다.
				// 이전 강의의 일반 로그인과 동일한 방식으로 로그인한 것으로 간주하여 처리합니다.
				// 기존 인증이 날아가기 때문에 OAUTH ROLE은 증발하며, USER ROLE 이 적용됩니다.

			}
			// 연결된 계정이 없는경우
			else {

				user = (User) auth.getPrincipal();
				user.setValidEmail(true);
				user = userManager.signUp(user);

			}
			SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities()));
			res.sendRedirect("/");
			sessionToken.set(req, user);

			signLogs.set(req, user);
			signLogs.setId(null);
			signLogs.setSignType(((User) auth.getPrincipal()).getSignType());
			countryUpdator.update(signLogs, signLogsManager);
		}
		sessionTokenManager.saveAndFlush(sessionToken);
	}
}
