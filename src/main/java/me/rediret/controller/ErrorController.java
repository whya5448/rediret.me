/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by 안병길 on 2017-02-25.
 * Whya5448@gmail.com
 */

@Controller("/error")
public class ErrorController {

	@RequestMapping("/403")
	public String AccessDenied() {
		return "/403";
	}

	@RequestMapping("/404")
	public String NotFound() {
		return "/404";
	}

}
