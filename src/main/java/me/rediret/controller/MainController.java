/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.controller;

import lombok.extern.slf4j.Slf4j;
import me.rediret.auth.user.User;
import me.rediret.url.URL;
import me.rediret.url.UrlManager;
import me.rediret.utils.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The type Main Controller.
 */
@Controller
@Slf4j
public class MainController {

	private final UrlManager urlManager;
	private Message message;

	@Autowired
	public MainController(UrlManager urlManager) {
		this.urlManager = urlManager;
	}

	/**
	 * Main string.
	 *
	 * @return the string
	 */
	@RequestMapping({"/", "index.html"})
	public String main() {
		return "main";
	}

	@PreAuthorize("hasAuthority('USER')")
	@RequestMapping("/links")
	public String links() {
		return "links";
	}

	@PreAuthorize("hasAuthority('USER')")
	@RequestMapping(value = "/links", params = {"page", "size", "sort", "ajax"})
	public
	@ResponseBody
	Message links(Pageable pageable, @RequestParam(required = false) String keyword, Authentication auth) {
		message = new Message();

		int userId = ((User) auth.getPrincipal()).getId();
		if (pageable.getPageNumber() < 0) {
			message.setStatus(HttpStatus.BAD_REQUEST);
			return message;
		}

		message.setStatus(HttpStatus.OK);
		message.setMessage("Query Compleated");
		Page<URL> list;
		if (keyword == null) list = urlManager.findByUserId(userId, pageable);
		else list = urlManager.findByUserIdAndLongUrlContainingOrShortUrlContaining(userId, keyword, pageable);
		message.put("links", list);
		return message;
	}
}
