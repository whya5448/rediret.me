/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.controller;

import lombok.extern.slf4j.Slf4j;
import me.rediret.url.URL;
import me.rediret.url.UrlManager;
import me.rediret.url.logs.RedirectURLAccessLogs;
import me.rediret.url.logs.RedirectURLAccessManager;
import me.rediret.utils.HttpError;
import me.rediret.utils.country.CountryUpdator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Created by 안병길 on 2017-01-03.
 * Whya5448@gmail.com
 */
@Controller
@Slf4j
public class RedirectController {


    private final HttpError httpError;
    private final RedirectURLAccessManager accessManager;
    private final CountryUpdator contryUpdator;
    private final UrlManager urlManager;

    /**
     * Instantiates a new Redirect controller.
     *
     * @param httpError
     * @param accessManager
     * @param contryUpdator
     * @param urlManager
     */
    @Autowired
    public RedirectController(HttpError httpError, RedirectURLAccessManager accessManager, CountryUpdator contryUpdator, UrlManager urlManager) {
        this.httpError = httpError;
        this.accessManager = accessManager;
        this.contryUpdator = contryUpdator;
        this.urlManager = urlManager;
    }

    /**
     * Redirect string.
     *
     * @param link the link
     * @return the string
     */
    @RequestMapping("/{link}")
    public String redirect(@PathVariable String link, HttpServletRequest request) {
        URL URL = null;
        List<me.rediret.url.URL> URLList = urlManager.findByShortUrl(link);
        if (URLList == null) throw httpError.new ResourceNotFoundException();

        switch (URLList.size()) {
            case 0:
                throw httpError.new ResourceNotFoundException();
            case 1:
                URL = URLList.get(0);
                break;
            default:
                for (me.rediret.url.URL entity : URLList)
                    if (entity.getShortUrl().equals(link)) {
                        URL = entity;
                        break;
                    }
        }

        if (URL == null) throw httpError.new ResourceNotFoundException();

        String longUrl = URL.getLongUrl();
        if (!longUrl.startsWith("http")) longUrl = "http://" + longUrl;
        log.info(longUrl);
        URL.setLastAccess(new Date());
        URL.setAccessCount(URL.getAccessCount() + 1);
        urlManager.saveAndFlush(URL);

        var redirectURLAccessLogs = new RedirectURLAccessLogs(request, URL);
        accessManager.saveAndFlush(redirectURLAccessLogs);
        contryUpdator.update(redirectURLAccessLogs, accessManager);

        return "redirect:" + longUrl;
    }
}
