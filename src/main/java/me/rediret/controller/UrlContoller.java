/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.controller;

import me.rediret.auth.user.User;
import me.rediret.auth.user.UserRepository;
import me.rediret.url.URL;
import me.rediret.url.UrlManager;
import me.rediret.url.logs.RedirectURLAccessManager;
import me.rediret.utils.Message;
import me.rediret.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Created by 안병길 on 2017-01-01.
 * Whya5448@gmail.com
 */
@RestController
public class UrlContoller {

    private final UrlManager urlManager;
	private final UserRepository userRepository;
	private final RedirectURLAccessManager redirectURLAccessManager;
	private final ResourceBundleMessageSource resourceBundleMessageSource;
	private Message message;


	@Autowired
	public UrlContoller(UrlManager urlManager, UserRepository userRepository, RedirectURLAccessManager redirectURLAccessManager, ResourceBundleMessageSource resourceBundleMessageSource) {
		this.urlManager = urlManager;
		this.userRepository = userRepository;
		this.redirectURLAccessManager = redirectURLAccessManager;
		this.resourceBundleMessageSource = resourceBundleMessageSource;
	}

	/**
	 * Save Url.
	 *
	 * @param URL the Url
	 * @return the Url
	 */
	@RequestMapping("/url/save")
	public Message save(URL URL) {
		message = new Message();

		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user;

		String urlTest = "^(https?://)?(www\\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_+.~#?&//=]*)";
		if (!URL.getLongUrl().matches(urlTest)) {
			message.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			message.setMessage(resourceBundleMessageSource.getMessage("main.urlunable", null, LocaleContextHolder.getLocale()));
			return message;
		}

		// 유저 정보 얻어옴
		if (auth.getPrincipal() instanceof String && auth.getPrincipal().equals("anonymousUser")) user = userRepository.findByEmail("rediret@rediret.me");
		else user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		// 시간당 60개 제한.
		Date d = new Date();
		d.setTime(d.getTime() - 3600_000);
		Long count = urlManager.countBycreatedDateGreaterThan(d);
		if (count >= 60) {
			message.setStatus(HttpStatus.TOO_MANY_REQUESTS);
			message.setMessage("Requests cannot over 60 at hour");
			return message;
		}

		URL.setShortUrl(urlManager.compress(urlManager.getRandInt(Integer.MAX_VALUE)));

        int i = 0;
        do {
	        List<me.rediret.url.URL> u = urlManager.findByShortUrl(URL.getShortUrl());
	        if (u == null || u.size() == 0) break;
	        URL.setShortUrl(urlManager.compress(urlManager.getRandInt(Integer.MAX_VALUE)));
	        i++;
        } while (i < 5);

		URL.setIp(Utils.getRealIp(req));
		URL.setCreatedDate(new Date());
		URL.setUser(user);
		URL = urlManager.save(URL);
		message.setStatus(HttpStatus.OK);
		message.setMessage(URL.getShortUrl());
		message.put("url", URL);

		return message;
	}

	@PreAuthorize("hasAuthority('USER')")
	@RequestMapping("/url/analyze")
	public Message analyze(URL url, Authentication auth) {
		message = new Message();

		if (!urlManager.exists(url.getId()) || urlManager.getOne(url.getId()).getUser().getId() != ((User) auth.getPrincipal()).getId()) {
			message.setStatus(HttpStatus.NOT_FOUND);
			return message;
		}

		message.setStatus(HttpStatus.OK);
		message.put("accessDate", redirectURLAccessManager.getAccessDateByURL(url));
		message.put("browserAndPlatform", redirectURLAccessManager.getUserAgentAndPlatformByURL(url));
		message.put("referrer", redirectURLAccessManager.getReferrerByURL(url));
		return message;
	}

}
