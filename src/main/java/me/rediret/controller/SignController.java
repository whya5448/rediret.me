/*
 * 안병길
 * Whya5448@gmail.com
 * https://gitlab.com/whya5448/rediret.me
 * Copyright (c) 2017.
 */

package me.rediret.controller;

import lombok.extern.slf4j.Slf4j;
import me.rediret.auth.user.User;
import me.rediret.auth.user.UserManager;
import me.rediret.auth.user.token.TokenState;
import me.rediret.auth.user.token.UserActivateToken;
import me.rediret.auth.user.token.UserActivateTokenManager;
import me.rediret.utils.Message;
import me.rediret.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Created by 안병길 on 2017-01-20.
 * Whya5448@gmail.com
 */
@Controller
@RequestMapping("/sign/")
@Slf4j
public class SignController {


	/**
	 * The Password encoder.
	 */
	private final UserManager userManager;
	private final UserActivateTokenManager userActivateTokenManager;
	private final Utils utils;
	private Message message;

	@Autowired
	public SignController(UserManager userManager, UserActivateTokenManager userActivateTokenManager, Utils utils) {
		this.userManager = userManager;
		this.userActivateTokenManager = userActivateTokenManager;
		this.utils = utils;
	}

	/**
	 * Sign in string.
	 *
	 * @return the string
	 */
	@RequestMapping("in")
	public String signIn() {
		return "/sign/sign_in";
	}

	/**
	 * Signup string.
	 *
	 * @return the string
	 */
	@RequestMapping("up")
	public String signUp() {
		return "/sign/sign_up";
	}

	@RequestMapping(value = "up", method = RequestMethod.GET, params = "email")
	@ResponseBody
	public Message signUp(@RequestParam String email) {
		message = new Message();
		boolean b = userManager.findByEmail(email) == null;
		message.setStatus(b ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
		message.setMessage(utils.getLocaleMessage(b ? "app.sign.up.email.available" : "app.sign.up.email.used"));
		return message;
	}

	@RequestMapping(value = "up", method = RequestMethod.POST)
	@ResponseBody
	public Object signup_put(@ModelAttribute User user) {
		message = new Message();
		if (userManager.findByEmail(user.getEmail()) != null) {
			message.setStatus(HttpStatus.IM_USED);
			message.setMessage(utils.getLocaleMessage("app.sign.up.email.used"));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
		}

		try {
			userManager.signUp(user);
		} catch (DataIntegrityViolationException e) {
			// 서버오류, 유니크 설정했는데 중복되는 경우.. 그 외에도 나온다곤 하는데 잘 모르겠음.
			message.setStatus(HttpStatus.IM_USED);
			message.setMessage(utils.getLocaleMessage("app.sign.up.email.used"));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
		} catch (Exception e) {
			message.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			message.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(message);
		}

		message.setStatus(HttpStatus.OK);
		message.setMessage(utils.getLocaleMessage("app.sign.up.success"));
		return message;
	}

	@RequestMapping("done")
	public String done() {
		return "/sign/done";
	}


	/**
	 * 이메일 인증 / 계정 활성화.
	 *
	 * @param token
	 */
	@RequestMapping("activate/{token}")
	public ModelAndView activate(@PathVariable String token) {
		UserActivateToken t = userActivateTokenManager.findByToken(token);
		ModelAndView mav = new ModelAndView("/sign/token");
		mav.addObject("head", utils.getLocaleMessage("app.sign.up.auth.token.head.error"));
		if (t == null) {
			// 존재하지 않는 토큰 페이지
			mav.addObject("msg", utils.getLocaleMessage("app.sign.up.auth.token.invalid"));
			return mav;
		}

		if (t.getTokenState() != TokenState.AVAILABLE) {
			// 만료되었거나, 재발급, 사용된 토큰.
			mav.addObject("msg", utils.getLocaleMessage("app.sign.up.auth.token.invalid"));
			return mav;
		}

		// 유효기간 체크
		LocalDateTime expired = LocalDateTime.ofInstant(t.getExpired().toInstant(), ZoneId.systemDefault());
		if (LocalDateTime.now().isAfter(expired)) {
			t.setTokenState(TokenState.EXPIRED);
			userActivateTokenManager.saveAndFlush(t);
			// 만료된 토큰 / 재발급 루트 만들어야함.
			mav.addObject("msg", utils.getLocaleMessage("app.sign.up.auth.token.expired"));
			return mav;
		}

		User u = t.getUser();
		u.setValidEmail(true);
		userManager.saveAndFlush(t.getUser());
		t.setTokenState(TokenState.USED);
		userActivateTokenManager.saveAndFlush(t);

		// 인증완료 로긴하세요 페이지
		mav.addObject("head", utils.getLocaleMessage("app.sign.up.auth.token.head.done"));
		mav.addObject("email", t.getUser().getEmail());
		mav.addObject("msg", utils.getLocaleMessage("app.sign.up.success"));
		return mav;
	}

}
